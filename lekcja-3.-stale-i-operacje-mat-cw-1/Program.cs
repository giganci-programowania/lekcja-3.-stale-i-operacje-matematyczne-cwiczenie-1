﻿using System;

namespace lekcja_3._stale_i_operacje_mat_cw_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string ulbioneWakacje = "Monako";
            int wiekKota = 12;
            Console.WriteLine("Wakacje {0}, wiek kota {1}", ulbioneWakacje, wiekKota);

            ulbioneWakacje = "Rodos";
            wiekKota = 17;
            Console.WriteLine("Wakacje {0}, wiek kota {1}", ulbioneWakacje, wiekKota);

            const string najgorszeWakacje = "Lubiatowo";
            const int numerButa = 44;
            Console.WriteLine("Najgorsze wakacje {0}, numer buta {1}", najgorszeWakacje, numerButa);

            // Tutaj będzie pierwszy błąd związany z próbą modyfikacji stałej
            najgorszeWakacje = "Dominikana";
            // A tutaj drugi
            numerButa = 46;

            Console.ReadKey();
        }
    }
}
